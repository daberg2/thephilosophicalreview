+++
# Custom widget.
# An example of using the custom widget to create your own homepage section.
# To create more sections, duplicate this file and edit the values below as desired.
widget = "custom"
active = true
date = 2016-04-20T00:00:00

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Submissions"
subtitle = ""

# Order that this section will appear in.
weight = 60

+++

Submissions should be sent to submissions@thephilosophicalreview.com. It is highly recommended that authors ensure that the submission meets the criteria listed below.

# Submission Guidelines

Submitted manuscripts should be PDF files prepared for anonymous review, containing no identifying information.

Submissions need not conform to the journal style unless and until accepted for publication.

There is no word limit, but submissions on the lengthier side should have correspondingly more news value.

To submit a paper, please register and login to&nbsp;_The Philosophical Review&#8217;s_&nbsp;editorial management system at.

## Accepted Submissions

Accepted papers must be adapted to the journal’s house style. The final document should be a Word file (.doc or .docx). The text should be structured consistently. The name and affiliation of the contributor(s) should appear at the top, along with an abstract.

_The Philosophical Review_&nbsp;follows MLA, or Chicago&nbsp;with the following exceptions:

**Citations with link:&nbsp;**Authors who want their in-text citations rendered as hyperlinks to the list of references must, if submitting a .doc/.docx file, color all in-text citations so that they can be identified during production. (Authors submitting .tex files can ignore this step.) Blue, or any color not used for another purpose elsewhere in the text, will do. We strongly encourage authors to make their in-text citations linkable, but it is not required.

References in footnotes and the References list:&nbsp;If a work has multiple authors or editors, the word&nbsp;_and_&nbsp;should be written before the last contributor’s name. At the beginning of the entry or in lists with three or more names, the word&nbsp;_and_&nbsp;is always preceded by a comma.

Page numbers are never introduced by&nbsp;_p_. or&nbsp;_pp_.

### **Examples (MLA):**

<p style="margin-left:.5in;text-indent:-.5in">
  Adams, Robert Merrihew. &#8220;<em>Motive</em>&nbsp;<em>U</em><em>tilitarianism</em>.&#8221; The Journal of Philosophy 73.14 (1976): 467-481.<br />
</p>

<p style="margin-left:.5in;text-indent:-.5in">
  Berg, Dustin. &#8220;<a href="https://thephilosophicalreview.com/wp-content/uploads/2018/09/A_Theory_of_Artificial_Classification_Th-1.pdf"><em>A Theory of Artificial Classification: The Development of Naturality from Artificiality.</em></a>&#8221; The University of North Carolina at Greensboro, 2018.<br />
</p>

<p style="margin-left:.5in;text-indent:-.5in">
  Lewis, David. &#8220;<em>On The Plurality of Worlds</em>.&#8221; Oxford 14 (1986): 43.<br /><br />
</p>

<p style="margin-left:.5in;text-indent:-.5in">
  &#8212;.<em>Counterfactuals</em>. John Wiley & Sons, 2013.
</p>

### **Examples (Chicago):**  


<p style="margin-left:.5in;text-indent:-.5in">
  Adams, Robert Merrihew. &#8220;Motive utilitarianism.&#8221;&nbsp;<em>The Journal of Philosophy</em>&nbsp;73, no. 14 (1976): 467-481.<br />
</p>

<p style="margin-left:.5in;text-indent:-.5in">
  Berg, Dustin. &#8220;<a href="https://thephilosophicalreview.com/wp-content/uploads/2018/09/A_Theory_of_Artificial_Classification_Th-1.pdf"><em>A Theory of Artificial Classification: The Development of Naturality from Artificiality.</em></a>&#8221; The University of North Carolina at Greensboro, (2018).<br />
</p>

<p style="margin-left:.5in;text-indent:-.5in">
  Lewis, David. &#8220;<em>On the Plurality of Worlds</em>.&#8221; Oxford 14 (1986): 43.<br />
</p>

<p style="margin-left:.5in;text-indent:-.5in">
  —<em>Counterfactuals</em>. John Wiley & Sons, 2013.
</p>

<div style="height:40px" aria-hidden="true" class="wp-block-spacer">
</div>

### Capitalization of headlines, titles, and subtitles:

In headlines and when citing titles and subtitles of other works in the English language, consistently capitalize as follows:

Capitalize all nouns, pronouns, verbs, adjectives, adverbs, and subordinating conjunctions.

_Do not_&nbsp;capitalize articles, prepositions, coordinating conjunctions, or the&nbsp;_to_&nbsp;in infinitives unless they are the first word in a headline, title, or subtitle. Sections should be numbered.

Acknowledgements and figures:&nbsp;Acknowledgments should appear in their own section before the references. Figures should use fonts that match the main text (Palatino for HTML, Sabon for PDF).

For any further stylistic concerns, refer to the [_MLA Style Center_][1], or&nbsp;[_The Chicago Manual of Style_][2].&nbsp;

British English or American English may be used, but either one should be followed consistently throughout the article. If the text is written in American English, spelling should conform to American English as exemplified in&nbsp;_Merriam-Webster’s Collegiate Dictionary_&nbsp;(2005) or&nbsp;_Webster’s Third New International Dictionary_&nbsp;(2002). If the text is written in British English, spelling should conform to the&nbsp;_Oxford English Dictionary_&nbsp;(1989), and Oxford spelling (-ize instead of -ise) should be used.

 [1]: https://style.mla.org/
 [2]: https://www.chicagomanualofstyle.org/home.html
