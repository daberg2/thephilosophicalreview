+++
title = "'Artificiality' and Meaning"
date = 2018-12-05T00:00:00
tags = ["Philosophy of Language"]
authors = ["D. A. Berg"]

# List format.
#   0 = Simple
#   1 = Detailed
#   2 = Stream
list_format = 2

# Optional featured image (relative to `static/img/` folder).
[header]
image = ""
caption = ""
+++


Philosophy of language examines language as a median between humans and the natural world. Subjects pertaining to philosophy of language include the study of composition, semantics and logic, reference, and much more. This essay, however, is primarily concerned with *meaning*. An important note for this section is that the uses of the term ‘language’ are not limited to spoken or written word.


Instead, references to language should be considered as the means by which organisms gather and present information. In this case, language serves as the mechanism by which humans make sense of the universe. Before examining the meaning of ‘artificiality’, this section will detail the etymology of the word, and identify the time period in which it became entrenched in negative connotations.

## Linguistic Philosophy of ‘Artificiality’

What is it that distinguishes humanity from the natural world? Some may argue that the human ability to rationalize, or that our intelligence is what sets us apart. Others may argue that humans are the product of divine creation, and that God bestowed the natural world to humanity.  Regardless of the position taken, the method by which we distinguish humanity and its creations from the natural world is through language. Language through signification, classification, and interpretation (or association) is what allows humanity to be able to distinguish one thing from another, and then communicate the difference to others. There is, however, a more direct linguistic classification that distinguishes humanity from the natural world: ‘artificiality’ and ‘naturality’.

Linguistic signs are created to distinguish objects from one another and to share that distinction via common language. The meaning of a sign consists of the essential properties of the referent. For example, zebras and horses share certain properties, but the respective signs share no meaning. Naturally, if the sign in question was _Equus_ then the meaning would be shared. The difference of the signs (zebra and horse) linguistically involves the difference in their meanings, their essential properties.

Linguistic philosophy is concerned with the use of words within problems of philosophy. This is, however, not to be confused with philosophy of language. As John Searle discusses in his book _Speech Acts_, the distinction between the two fields of philosophy is sharp:

> I distinguish between the philosophy of language and linguistic philosophy. Linguistic philosophy is the attempt to solve particular philosophical problems by attending to the ordinary use of particular words or other elements in a particular language. The philosophy of language is the attempt to give philosophically illuminating descriptions of certain general features of language, such as reference, truth, meaning, and necessity; and it is concerned only with particular elements in a particular language; though its method of investigation, where empirical and rational rather than _a priori_ and speculative will naturally force it to pay strict attention to facts of actual languages.
>
> <cite>Searle, John R. <em>Speech Acts: An Essay in the Philosophy of Language.</em></cite>

Thus, the distinction between the two fields is well defined. However, it will be necessary for this study of artificiality to utilize both methods of inquiry. In this section, I will detail the etymology and provide additional sources regarding the origin of the word ‘artificial.’

According to the etymology in the Oxford English Dictionary, ‘artificial’ has multiple origins, but a very singular and arbitrary definition:  

-   Anglo-Norman and Middle French ‘artificiel’ (French artificiel )
-   Skillfully made or contrived,
-   Brought about by human skill or intervention (1267 in Old French),
-   Cunning, seeking to deceive (1532),
-   Unspontaneous, affected (c1537) and
-   Its etymon classical Latin artificiālis made or contrived by art (used by Quintilian in rhetorical context, translating ancient Greek ἔντεχνος )

While the etymological tracing of the definition shows some variability in content, the reoccurring theme is that ‘artificial’ is a result of human action, including acts of creation, intervention, alteration, and so forth. The notion of artificiality most likely stems from the word ‘artifice’, a creation of humankind.

But, _<u>why</u>_ go through the philosophical rigmarole of making a distinction between human/human creations and the natural domain?

---

## Apprehension to the Artificial


The term ‘artificial’ is commonly used to represent something that has been altered by human action, or as a reference to a creation that copies another object. Yet, upon a closer examination of the linguistic use of ‘artificial’, the line between natural or artificial becomes blurred and ambiguous. Unfortunately, the problem of artificiality does not cease at ambiguity. Instead, the problem entrenches itself into negative connotations that bring about a wide-variety of emotional responses to science and the “unnatural.”

Emotional responses that often become amplified within an echo chamber of rhetoric and political gamesmanship. Sciences such as quantum physics identified the presence of fundamental particles shared across various organisms previously categorized as distinctly different, thus bringing humanities uniqueness under question. Artificiality, then, is an improper justification for a rejection of technological and scientific pursuits.  

But, where (or when) did this linguistic sign become entrenched into these negative connotations?

### Tracing the Artificial Domain

According to Rafael Capurro, the starting place for the distinction between artificial and natural is the ancient Greek philosophy. “For the Greeks, generally speaking, there are some things which grow up as a product of nature (physis), and other things which are produced by man (poiesis), such as tools, machines, and works of art” (Capurro, I, 1). Gregor Schiemann provides a similar origin of ‘artificial’, “The distinction between nature and technology is among the oldest conceptions of nature in European cultural history. One of its formulations traces back to Aristotle, who defines technology (techne) as what occurs in the case of human production and owes its existence to human production.  In contrast, Aristotle considers nature (physis) as that reality which changes or moves on its own (Schiemann, 101).

Google’s “Ngram Viewer” is a system designed to provide word-usage statistics in literature over a specified time-period. Using the program, I conducted a search for ‘artificial’: the use of ‘artificial’ increases from 0.0000201645% in 1581, to 0.0014392734% in 1667. (Google, Ngram Viewer). While the percentage appears minuscule, the data indicates an increase of 7,038% over 86 years. It is worthwhile to note that which is probably obvious, the Google Ngram Viewer only counts words within the literature it maintains in its database. So, while the accuracy of those statistics may be under question, the statistical trend of word-use should not be.

In the mid-17th century, major political shifts and theories were underway, to include the writing of _Leviathan_. Additionally, later in the century important technological developments occurred, such as the steam engine in. This created efficiency in production, and subsequently established new demands for coal and iron. Other developments such as legal innovations included patents for inventions, banking methods, and trade-agreements. Of those, the development of banking is the most significant. Banking allowed for statistical information in economics, and for the collection and redistribution of old and new tender (Wiesner-Hanks, 472).

This indicates a shift from the previously natural way of living for Europeans during the early modern era. These changes in social and economic dynamics would likely have been daunting, and certainly have led to a greater inequity. Further, with the development of steam engines and the subsequent need for greater weaponry in warfare eventually lead to the grotesque scene of trench warfare in World War I.

The point, here, is that the sharp increase in the use of the word ‘artificial’ coincides within an era of dramatic social, political, and economic changes. It is possible that the entrenchment of the word into negative connotations began in the mid-17th century.  Death and destruction has come in tandem with the development of the industrial world and changed the way that humanity would view the environment. Humanity would set down a path of estrangement from nature due to its dependence on machines and coin.
