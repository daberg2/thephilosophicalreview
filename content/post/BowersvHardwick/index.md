+++
title = "On the Legitimacy of the Court in Bowers v. Hardwick"
date = 2018-11-09T00:00:00
authors = ["D. A. Berg"]
tags = ["Social and Political Philosophy"]

# List format.
#   0 = Simple
#   1 = Detailed
#   2 = Stream
list_format = 2

# Optional featured image (relative to `static/img/` folder).
[header]
image = ""
caption = ""
+++

In March of 1986, Michael Hardwick brought forward a suit in the Federal District Court “challenging the constitutionality of the statute insofar as it criminalized consensual sodomy” (Bowers V. Hardwick, 478 U.S. 186z). Preceding the court case, Hardwick was charged with violating Georgia’s law against sodomy when a police officer observed Mr. Hardwick conducting sexual activity with another adult male within the privacy of his own home. Three months following the oral argument, the Supreme Court decided that Hardwick failed to make a claim, and subsequently ruled that there are no constitutional protections for individuals conducting acts of sodomy. I believe that Court made monumental errors in their decision.
<figure>
    <img style="float: left;" alt="Michael Hardwick (left)" src="/img/BvH2.jpeg">
    <figcaption>Michael Hardwick (left)</figcaption>
</figure>
First, the basis for the Court’s decision rests upon the notion that protecting acts of sodomy within private locations would “be the product of “judge-made constitutional law” and send the Court down the road of illegitimacy” [(Oyez)](https://www.oyez.org/cases/1985/85-140). The failure of the Court in this statement is the assumption that the 14th amendment precludes acts of sodomy. A second and equally monumental failure of the Court’s decision is the violation of the First Amendment through religious interpretations of ‘sodomy’.

## The Road to Illegitimacy

In the opinion delivered by Justice White, he argues that the Court would become vulnerable to illegitimacy when dealing with judge-made law:

>The Court is most vulnerable and comes nearest to illegitimacy when it deals with judge-made constitutional law having little or no cognizable roots in the language or design of the Constitution. That this is so was painfully demonstrated by the face-off between the Executive and the Court in the 1930&#8217;<img style="float: right;" alt="Justice White" src="/img/white.jpg">s, which resulted in the repudiation (478 U.S. 186, 195) of much of the substantive gloss that the Court had placed on the Due Process Clauses of the Fifth and Fourteenth Amendments. [(188)](https://www.loc.gov/item/usrep478186/)
>
><cite>Justice White, Opinion of the Court</cite>

Justice White’s opinion demonstrates a slippery-slope argument on the basis that a decision to rule against Georgia law, would necessarily create a cascading effect of appeals to rule against other state law. White’s qualifying remarks for his claim is that there should be “great resistance to expand the substantive reach of those Clauses, particularly if it requires redefining the category of rights deemed to be fundamental […&#8230;] The claimed right pressed on us today falls for short of overcoming this resistance” (188).

Certainly, there should be such resistance against expanding federal authority over state rule. The problem in Justice White’s opinion is that there _was_ substantial reason to overcome the resistance. Hardwick’s lack of an identifiable claim is not sufficient reason to turn a blind eye to the violation of the right to privacy as interpretably granted by the 14th Amendment. Further, Hardwick’s apparent lack of a claim is the result of the Court majority’s inability to appropriately interpret the constitution. Instead, the majority used strict-constructionism to come to the conclusion that Hardwick’s claim consists of “rights not readily identifiable” (188).

However, the notion that the constitution should be strictly interpreted, and thus only grant explicitly stated rights, is mediocre. If the constitution were inflexible, society would be locked in a moral purgatory, damned for eternity to treat people of different races, sexes, and sexuality with varying rights. Fortunately, this is not the case. Individuals with the ability to rationalize the constitution with the evolution of societal norms have effectively shown that the constitution is capable to evolve in tandem. The dissenting opinion of the Court argues precisely against this type of strict-constructionist interpretation:

>This case is no more about “a fundamental right to engage in homosexual sodomy,” as the Court purports to declare, ante, at 191, than Stanley v. Georgia, 394 U.S. 557 (1969), was about a fundamental right to watch obscene movies, or Katz v. United States, 389 U.S. 347 (1967), was about a fundamental right to place interstate bets from a telephone booth. Rather, this case is about “the most comprehensive of rights and the right most valued by civilized men,” namely, “the right to be let alone.” Olmstead v. United States, 277 U.S. 438, 478 (1928)
>
><cite>Justice Blackmun, Dissenting Opinion</cite>

The strict-constructionist decision fails to adequately establish proper rule of law, and, ironically, brings the Court’s status of legitimacy into question. Additionally, the decision appears to violate the 1st Amendment, which is the discussion of the following section.

## Religious Interpretations of ‘Sodomy’

‘Sodomy’ is typically defined as the act of oral or anal sex. The word ‘sodomy’ has a latin origin of _peccatum Sodomiticum,_ which is loosely translated to “the sin of sodom”. Sodomy laws are based on religious interpretations which dogmatically claim that acts of sodomy are destructive to the moral fabric of society. However, [according to David Catania](https://heinonline.org/HOL/LandingPage?handle=hein.journals/amcrimlr31&div=19&id=&page=), advocates of this interpretation have no evidence to support the claim (293). The Court’s decision claims that there is not any indication within the constitution that protects an individual’s privacy to have sex with another consenting adult, if the sex is within the definition of sodomy. Religion has a long history of establishing morals and laws within civilized societies. So, it is unsurprising to find remnants of archaic definitions within the rule of law.

The Court’s decision to uphold Georgia sodomy laws subsequently uphold the archaic and religious definition. The Court’s majority failed to properly examine the term and ask themselves “Why?”. Why should any form of government decide what consenting adults do within the privacy of their own home? Unfortunately, the answer to this question would go unanswered for 17 years, until the Supreme Court voiced their opinions in 2002 on [Lawrence V. Texas](https://www.oyez.org/cases/2002/02-102) [(539 U.S. 558)](https://supreme.justia.com/cases/federal/us/539/558/).

Yet, not asking themselves this important question is not the only failure of the Court’s majority. They also failed to consider the implications of upholding a law based on religious definitions. Simply being a part of the “Bible belt” does not exempt a state from the 1st Amendment. Undoubtedly, had the Supreme Court issued a rule which enforces a religion other than Christianity, the state of Georgia would decry the violations of the 1st Amendment. Therefore, it is unacceptable and hypocritical for the state government to enforce law based on religious interpretations, and tenfold for the Supreme Court to uphold such a law.

An argument may be raised here, claiming that the Supreme Court ruled according to the social norms of the time. This argument would seemingly damage my claims thus far, considering I had mentioned in the previous section that the constitution is capable of evolving in tandem with society. However, the point is not that Court decisions should be made in accordance with societal norms, but rather decided in favor of all humans when the issue is brought forward. There are times when society demands change, and there are times when society must be thrust forward toward change. In _Bowers V. Hardwick_, it was the latter that was necessary.

The Supreme Court decision on _Bowers V. Hardwick_ goes against the very foundation of the constitution. The majority opinion delivered by Justice White chooses to favor a strict interpretation of the constitution over pragmatic, and analytical. The dissenting opinion shows that the issue was not concerned primarily of sodomy, but rather the right to individual privacy. Justice White claimed that ruling against the state in this case would result in judge-made constitutional law and bring the Court’s legitimacy under question.

However, White failed to consider that the decision resulted in the very thing he hoped to avoid. By not acting with pragmatism, and by enforcing religious definitions within the law, the Supreme Court failed in its most basic duty; to ensure equal rights to all citizens as granted by the constitution. Interpreting the constitution requires that consideration be given to the rights of all humans, and determining how the Court decision can maximize those rights, not minimize them in favor of arbitrary reasoning.

### Further reading and references:

“Bowers V. Hardwick”. 478 U.S. 186. Supreme Court of the United States. 1986. U. of North Carolina at Greensboro, n.d. Web. 03 Feb. 2018.

-   [Oyez](https://www.oyez.org/cases/1985/85-140)
-   [Legal Information Institute (Cornell Law School)](https://www.law.cornell.edu/supremecourt/text/478/186)
-   [Encyclopedia Britannica](https://www.britannica.com/event/Bowers-v-Hardwick)

Catania, David A. “The Universal Declaration of Human Rights and Sodomy Laws: A Federal Common Law Right to Privacy for Homosexuals Based on Customary International Law,” American Criminal Law Review vol. 31, no. 2 (Winter 1994): p. 289-326

-   <https://heinonline.org/HOL/LandingPage?handle=hein.journals/amcrimlr31&div=19&id=&page=>

“Lawrence v. Texas.” 539 U.S. 558. Supreme Court of the United States. 2002. _U.S. Case Law,_ Justia, Web. 04 Feb. 2018

-   [Oyez](https://www.oyez.org/cases/2002/02-102)
-   [Legal Information Institute (Cornell Law School)](https://www.law.cornell.edu/supct/html/02-102.ZO.html)
-   [Encyclopedia Britannica](https://www.britannica.com/topic/Lawrence-v-Texas)
