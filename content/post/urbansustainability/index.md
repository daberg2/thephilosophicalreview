+++
title = "The 'Wicked-Problem' of Urban Sustainability"
date = 2018-11-28T00:00:00
authors = ["D. A. Berg"]
tags = ["Social and Political Philosophy"]

# List format.
#   0 = Simple
#   1 = Detailed
#   2 = Stream
list_format = 2

# Optional featured image (relative to `static/img/` folder).
[header]
image = ""
caption = ""
+++

## **Introduction**

New York, New York is often considered to be a polluted metropolis filled with smog and traffic. Nonetheless, NYC turned itself into one of the leading cities in urban sustainability. In 2007, New York City Mayor, Michael Bloomberg launched the ‘PlaNYC’ initiative. The goal: to drive down the city’s carbon dioxide emissions and to establish sustainability. As a part of PlaNYC, Mayor Bloomberg ordered an inventory of NYC’s emissions. 

In 2005 New York City was responsible for 58.3 million metric tons of carbon dioxide (Dickinson, 2007). Since NYC initiated the plan over a decade ago, we can easily analyze the success of the plan. According the inventory of 2015, New York City has reduced its emissions nearly 15% since 2005 (City of New York, 2017). The sign is clear, that the success of the policy has manifested in the reduction of greenhouse gas emissions.

Yet, there remains the problem of answering how it was that this became successful. The City of New York is home to a multitude of major corporations, cultures, and organizations. Each of which have their own goals and interests, and want policy to assist in reaching those goals. To further complicate the matter, these goals are often conflicting with ulterior goals. Thus resulting in a _wicked-problem_ for policy makers.

We examine the factors leading up to and beyond New York City’s initiatives. The first section identifies and explains the three major themes of urban sustainability. Those themes are: social, economic, and environmental. Then we can apply the three themes to the case of New York City. Lastly, we can identify the measures that the city has taken to ensure urban sustainability.



## **Urban Sustainability**

Urban sustainability has moved up in priorities for many local governments, and especially so for large cities. While there are a multitude of reasons that caused the sudden vault toward resource efficiency, the general causes are concerns over the impact of climate change, and the increasing population density in large cities (National Research Council, 2014). 

While these two causes appear separate, they are also symbiotic. As the population density increases, concentration of greenhouse gas emissions occurs in tandem. (National Research Council, 2014). This study involves an examination of social, environmental, and economic factors. Therefore, the study of urban sustainability is necessarily an interdisciplinary field.

Urban sustainability attempts to produce solutions that will create economic prosperity. Additionally, it makes the location a more comfortable place to live. [Daniel Childers][1] refers to this approach to as moving populations toward an “ecology for cities”.

Approaching each dimension exclusively will result in imbalanced trade-off. By focusing on one factor, the result is a dysfunctional urban infrastructure. For example, focusing on the economic dimension may create poor social sustainability. The focus of this section is to identify the challenges of each dimension for urban policy. 



<h3 style="text-align:center">
  Social Sustainability
</h3>

The NRC showed that cities produce 85% of the world&#8217;s greenhouse gas emissions.   


Continued advances in farming technology subsequently reduces the need for laborers. In short, this entails a migration of people moving to cities in search of occupations. Population density within cities has become an important concern. Local governments seek to limit urban sprawl yet maintain a thriving economy. Also, social sustainability is a key component to the puzzle of urban sustainability.

Social sustainability has ambiguous factors; cohesion, community, quality of life, safety, and so on. Each of the factors in social sustainability need individual solutions. Thus, further complicating generalized responses toward urban sustainability. According to Nicola Dempsey, there are two categories of social sustainability. The first is non-physical factors, and the second if predominantly physical factors. Dempsey further breaks down the categories into positive or negative. 

<h5 style="text-align:center">
  Non-Physical Factors
</h5>

The non-physical factors includes items such as education, social justice, and so on. Dempsey considers high-quality living environments, and social cohesion to be positive. While the contrapositives of each to be negative

<h5 style="text-align:center">
  Predominantly Physical Factors
</h5>

In the physical factors are items like urbanity, aesthetics, environmental quality, and neighborhoods

> •Urbanity  
> • Attractive public realm  
> • Decent housing  
> • Local environmental quality and amenity  
> • Accessibility (e.g. to local services and facilities/employment/  
> green space)  
> • Sustainable urban design  
> • Neighbourhood  
> • Walkable neighbourhood: pedestrian friendly 
> 
> <cite>Dempsey, 2011</cite>
While this particular view seems axiomatic, Dempsey is able to calculate the probability of a city’s success. When the negatives outweigh the positive, social chaos would likely entail. [Research suggests][2] that there are more factors for consideration. These include: trust, common meaning, diversity, capacity for learning, and capacity for self-organization. These factors seem to add a more in-depth political dimension to Dempsey’s categorization. The task for urban policy, then, is to create a system that supports the development of each of these factors.



<h3 style="text-align:center">
  Economic Sustainability
</h3>

Economic sustainability is the focal point of many local governments. They seek to maintain a flow of wealth into their cities. However, economic change no longer rests strictly in the hands of the government. Civic leaders and organizations capable to establish their community expectations. Fortunately, the interests both often include economic prosperity. The civic leaders are able to bring forth problems that governments often overlook.

According to [Kent Portney in his book][3], smart growth is a term which describes urban planning that attempts to avoid creating common problems. Problems such as urban sprawl, traffic congestion, declining public services, and abandoned and deteriorating inner-city infrastructure (Portney, 2013). Smart growth allows for local governments to continue to pursue economic prosperity and engage in multiple facets of sustainability.

<h5 style="text-align:center">
  The Problem for Urban Policy
</h5>

There is, however, an important limiting factor to the smart growth, or any growth, forms of economic development: state law (Portney, 2013). Cities are subordinates of the state, and are therefore restrained to the political powers that the state has granted them. Each state has different powers granted to local governments, implementing either the “home rule” or “Dillon’s Rule”. This variation among states creates a subsequent variation among cities and their capacities for being able to create effective, and worthwhile, changes. 

Portney identifies some changes that have been, or are in the planning stages of being, executed. Among them are zoning laws which limit land use and purchase to those who would be actively using it, thus defeating land abandonment and deterioration. Another is the development of eco-friendly parks that allow for partnerships among producers to share and recycle by-products and waste (Portney, 2013). Therefore, the implication for urban government is to establish policy oriented toward efficient and pragmatic resource management. 



<h3 style="text-align:center">
  Environmental Sustainability
</h3>

Environmental sustainability is the driving force behind most sustainability practices. Faced with surmounting climate changes and devastating weather, populations have begun to take notice of any government inactive in making change. As mentioned in the first section, cities produce approximately 85% of greenhouse gas emissions (National Research Council). Emissions, among other related subjects (i.e. energy and water use, and ecological footprint) are measured using a consumption-based approach, or also known as the extended input-output analysis (Baynes, Wiedmann, 2012). 

Urban green spaces have begun showing up in more cities, but are still few and far between (Wolch, 2014). Additionally, access to green zones has become mostly available to predominantly upper-middle-class, white communities, leaving poorer communities without (Wolch, 2014). This dynamic presented in Jennifer Wolch’s research article brings a new problem to environmental sustainability, and that is environmental justice. Thus, creating an additional link between social and environmental sustainability.

<h5 style="text-align:center">
  Corporate Participation
</h5>

A key factor in planning for environmental sustainability is corporate participation, such as that demonstrated in the eco-friendly parks in Portney’s book (Portney, 2013). Corporations and industries are often the largest consumers, and thus the largest producers of emissions. By including corporations in a “sharing of responsibility” the potential for environmental improvement is tenfold (Portney, 2013).

Policy considerations for environmental sustainability must encompass both of the previous sections, as they are the primary influences upon the environment. Local governments seeking to establish a balance between the three sections should establish policy that is rich with study in ‘urban ecology’ (Wu, 2014). 

However, the three sections discussed here cannot be approached individually. Instead, the social, economic, and environmental considerations and problems must be addressed holistically. Unfortunately, bringing the three dimensions together is rarely simple. Policy must be established that seeks an equilibrium among the three dimensions. 

To further complicate the matter, politicians are often pulled by their constituents and benefactors to enforce policy that supports only one of the dimensions, thus creating an imbalance rather than equilibrium. Therefore, not only do local governments have to seek to balance the three dimensions in policymaking, but they must also seek to unify their constituents behind the decision.

## **The Beginning of Sustaining NYC**

Now that the major factors of urban sustainability have been identified and placed within the scope of urban policy, we can now turn to utilize those factors in examining New York City’s successful drive for a sustainable urban environment. First, however, there is the necessity of developing the background and events leading up to the development of PlaNYC. Following this, each subsection will correlate to each of the themes previously presented.

<h3 style="text-align:center">
  Background
</h3>

Mayor Bloomberg announced his plans for NYC in 2007 under a program labeled “PlaNYC”. Bloomberg identified the problem with reactive policy and presented PlaNYC as a solution through proactive policy. During a conference on April 22nd, 2007 (also known as Earth Day), Mayor Bloomberg announced the purpose and goals for his plan for New York City, 

With historically low unemployment, a low crime rate and better schools, New York is thriving, it’s a place that people want to be. The time to build on our success is now, and I will not spend my last 984 days in office ignoring the problems that this City will face in the future. We need to start meeting the challenges we’ll face as we grow by nearly 1 million people, and we’ll do it by working to enact these 127 policy initiatives (Foreign Policy Association, 2007). 

<h5 style="text-align:center">
  Reactive Vs. Proactive
</h5>

Despite the guise of proactivity, the PlaNYC initiative was reactive in a few different ways. Most prominent of which, is the growing public cognizance of global warming and greenhouse gasses. Preceding the announcement, Mayor Bloomberg directed a gathering of emissions information from New York City, which concluded that in 2005 New York City was responsible for 58.3 million metric tons of carbon dioxide equivalent (Dickinson, 2007). 

Now that a decade has passed since the initiative’s announcement, there is an abundant amount of data to show the effectiveness of PlaNYC. Continuing with Mayor Bloomberg’s direction to continuously collect emissions data, the most recent report was released in 2017. The report uses the inventory of 2015, and indicates that New York City has reduced its emissions nearly 15% since 2005 (City of New York, 2017). 

Therefore, the policy’s effectivity shows clear advancements in environmental sustainability. It is not surprising that NYC is currently considered the leader in urban sustainability, and has established a model for other cities to mimic in their own attempts to achieve a sustainable urban environment (E2.org, 2018). The following section lays out a roadmap of the major factors.&nbsp;



<h3 style="text-align:center">
  Social Sustainability in NYC
</h3>

At the announcement of the initiative, PlaNYC was first viewed with skepticism. Major corporations saw the initiative as an attack on business rights, and produced criticism against the policy. Further, a large portion of the population in New York City are employees of those businesses, and additionally saw the initiative as a threat to their livelihood. This attitude toward drastic change continued for years following the announcement. However, in 2012, New York City would experience an environmental rarity that spurred the public desire for change; Hurricane Sandy. 

<h5 style="text-align:center">
  Reaction to Disaster
</h5>

Following the aftermath of Hurricane Sandy, Mayor Bloomberg announced an additional initiative that served as an augmented to PlaNYC,

> Six years ago, PlaNYC sounded the alarm about the dangers our city faces due to the effects of climate change and we’ve done a lot to attack the causes of climate change and make our city less vulnerable to its possible effects. But, Hurricane Sandy made it all too clear that, no matter how far we’ve come, we still face real, immediate threats. 
> 
> These concrete recommendations for how to confront the risks we face will build a stronger more resilient New York. This plan is incredibly ambitious and much of the work will extend far beyond the next 200 days, but we refused to pass the responsibility for creating a plan onto the next administration. This is urgent work, and it must begin now.
> 
> <cite>City of New York, 2013</cite>
Thus, PlaNYC became supercharged and revamped by Bloomberg’s desire for a “stronger more resilient New York”. 

<h5 style="text-align:center">
  Urban Housing
</h5>

A major consideration of PlaNYC to social sustainability is the plan for being able to house an additional 1 million residents. Further, the plan includes a holistic approach toward massive population growth, including decreased traffic congestion, additional health care facilities, and education reformations, 

> “We need to increase open space, expand housing, deal with our congested roadways, create better mass transit options, increase our energy sources and stabilize our water supply or we simply won’t be able to continue the high quality of life we now enjoy. &nbsp;If we act now, we’ll have a better future, a better quality of life, and more importantly, our children and their children will too”
> 
> <cite>City of New York, 2013</cite>
With the spur of public support, the initiative began making drastic changes toward the advancement of urban social sustainability. However, despite the high hopes of Mayor Bloomberg, some goals have yet to be reached. One problem that remains, is the seemingly immobile traffic in New York City. 

Another, is the growing poverty within the city’s poorer districts. [According to Steven Cohen][4], the current Mayor, Bill de Blasio, has communicated intentions to develop policy to handle the traffic congestion and poverty. But, “he has little power to achieve either of those goals”. The limited power to act stems from the national and state level policies in place. These bar attempts toward income redistribution.



<h3 style="text-align:center">
  Economic Sustainability in NYC
</h3>

As the major hub of financial corporations around the world, New York City consumes massive amounts of paper, and subsequently produces massive amounts of greenhouse gases. As mentioned in the previous section, the initial response toward PlaNYC by corporations was with skepticism and with defensive posturing. 

Any policy seeking to reduce the emissions within an area will undoubtedly seek to find measures to reduce the emissions of major corporations. Thus, the corporate entities justifiably believed that they would lose capital in PlaNYC’s implementation. This attitude toward the policy would continue to be the predominant one until the change in public attitude in the aftermath of Hurricane Sandy.

<h5 style="text-align:center">
  Economic Viability
</h5>

Reducing corporate consumption, emissions, and waste, inevitably resulted in a cost to corporations of NYC. However, economic advantage was given to the city through the plan to redevelop the city’s brownfield policy. Brownfields are areas that are either unutilized, underutilized, or considered “contaminated property” (U.S. Environmental Protection Agency, 2016).&nbsp;

As a part of PlaNYC’s intention to be able to house 1 million new residents, Mayor Bloomberg planned to establish a redevelopment plan for NYC’s brownfields by using the areas for housing. This plan allowed for cleaning of the areas through the Environmental Protection Agency’s plan, and new construction in sanitized areas.



<h3 style="text-align:center">
  Environmental Sustainability in NYC
</h3>

With the reduction of emissions by 15%, as indicated by the 2015 inventory, the City of New York is on path to continue to reduce emissions despite a growing population. Following the revamp of PlaNYC under the project OneNYC, policy has been introduced to produce more “green buildings” (City of New York, 2017). Additionally, the OneNYC plan adjusted the aims of emission goals to a reduction of a daunting 80% by 2050. 

<h5 style="text-align:center">
  Creating the Incentive to Change
</h5>

As a part of the effort to produce more green buildings, the City of New York has implemented an incentive toward projects that are considered to reduce emissions and waste. The incentive is a monetary stipend, which varies in value according to the project itself. 

The Department of Citywide Administrative Services (DCAS), has awarded $85.4 million thus far. According to the publication for OneNYC, the city has already established green buildings that or predicted to reduce emissions by 300,000 metric tons, and generate over 600 jobs.

The OneNYC plan also intends to utilize advancements in solar energy to reduce emissions. According to the NYC Office of Sustainability, Citywide solar capacity has surpassed 100 megawatts (MW), keeping the city on track to meet the goal of installing 100 MW of solar power on public buildings and spurring the installation of 250 MW on private buildings by 2025 (NYC Office of Sustainability, 2018). 



# Sustaining Urban Sustainability

The City of New York has established a model of urban sustainability for many cities, and currently leads the world in reducing emissions, while maintaining and improving corporate and social environments. With the revamped PlaNYC under OneNYC, the city has applied new methodology to harvest data and information to establish realistic, but nonetheless daunting, goals for the city. Setting a trend of reducing emissions by 15%, in conjunction with a growing population is no easy feat. 

In order to continue with the trend, New York City policy makers must remain cognizant of the multifaceted nature of urban sustainability, and include not only environmental concerns, but also public and economic concerns as well. The social, economic, and environmental considerations and problems must be addressed holistically, and policymakers must avoid obligations to entities whose interests will upset the equilibrium of the three primary factors. &nbsp;

## Further Reading and References:

<div id="zotpress-bf336012400d1987bd3c8c401c727916" class="zp-Zotpress zp-Zotpress-Bib">
  <span class="ZP_API_USER_ID" style="display: none;">5126438</span> <span class="ZP_ITEM_KEY" style="display: none;"></span> <span class="ZP_COLLECTION_ID" style="display: none;">FI95W8MI</span> <span class="ZP_TAG_ID" style="display: none;"></span> <span class="ZP_AUTHOR" style="display: none;"></span> <span class="ZP_YEAR" style="display: none;"></span> <span class="ZP_DATATYPE" style="display: none;">items</span> <span class="ZP_INCLUSIVE" style="display: none;">1</span> <span class="ZP_STYLE" style="display: none;"></span> <span class="ZP_LIMIT" style="display: none;"></span> <span class="ZP_SORTBY" style="display: none;">default</span> <span class="ZP_ORDER" style="display: none;">ASC</span> <span class="ZP_TITLE" style="display: none;"></span> <span class="ZP_SHOWIMAGE" style="display: none;"></span> <span class="ZP_SHOWTAGS" style="display: none;"></span> <span class="ZP_DOWNLOADABLE" style="display: none;"></span> <span class="ZP_NOTES" style="display: none;"></span> <span class="ZP_ABSTRACT" style="display: none;"></span> <span class="ZP_CITEABLE" style="display: none;"></span> <span class="ZP_TARGET" style="display: none;"></span> <span class="ZP_URLWRAP" style="display: none;"></span> <span class="ZP_FORCENUM" style="display: none;"></span> <span class="ZP_HIGHLIGHT" style="display: none;"></span> <span class="ZOTPRESS_PLUGIN_URL" style="display:none;">https://thephilosophicalreview.com/wp-content/plugins/zotpress/</span> 
  
  <div class="zp-List loading">
  </div>
  
  <!-- .zp-List -->
</div>

<!--.zp-Zotpress-->

 [1]: https://www.sciencedirect.com/science/article/pii/S0169204614000383
 [2]: https://www.sciencedirect.com/science/article/pii/S0959652616302645
 [3]: https://amzn.to/2wXvnlY
 [4]: https://www.huffingtonpost.com/steven-cohen/from-planyc-to-onenyc-new_b_7151144.html