+++
title = "Emergentism and the Extended Mind Theory"
date = 2018-11-19T00:00:00
authors = ["D. A. Berg"]
tags = ["Philosophy of Mind", "Cocnsiousness"]

# List format.
#   0 = Simple
#   1 = Detailed
#   2 = Stream
list_format = 2

# Optional featured image (relative to `static/img/` folder).
[header]
image = ""
caption = ""
+++

The human dependence upon sensory perception deceives the individual into assuming that spatial separation from an object, necessarily renders it as distinct from the individual mind. This article details the history and general theories of emergentism and the extended mind.

## British Emergentism

The theory of emergentism has roots dating as far back to John Stuart Mill. Mill kick-started emergentism with his work in [_System of Logic_][1]. He argued that heteropathic emergence is the result of physiological interactions.

Mill uses the analogy of, “A young boy may grow into an old man, but an old man may not grow into a young boy”. In general, Mill argued that a sum of properties can emerge from various causes that are _irreducible_ to the individual parts. Mill is the starting point for a wave of philosophers that concurred with him following his theory’s introduction. This wave is now known as “British Emergentism”.

A more recent example, but also considered as a member of the British emergentism group, is Charlie Dunbar (C.D.) Broad. Broad’s notion of emergentism was within the dualism domain. Physicalism, argues that mental properties are incompatible with physical properties. Thus leaving a large question mark on the causal role between mind and body. Broad, however, rejects this notion of incompatibility in substance. Instead he argued for the compatibility in properties.

<blockquote class="wp-block-quote">
  <p>
    <em>We now understand what is asserted in common by theories which make mentality and materiality both “differentiating attributes”. It is evident that, if we accept dualism, five alternative views are possible about the relations between mentality and materiality. [&#8230;] I therefore reject the Dualism of incompatibles, which was Descartes’ theory. We can therefore confine ourselves to the Dualism of Compatibles[&#8230;]. </em>
  </p>
</blockquote>

So, the pinnacle of the British emergentism movement ends on the note of emergentism being within the category of property dualism.

## Modern Emergentism

The theories of emergentism during the 19th and early 20th centuries met a swift end. The development of advanced scientific techniques favored physicalism and reductionism. To that end, humanity developed an intuition that sensory perception alone can provide life’s answers.

<img style="padding: 0 25px; float: left;" src="/img/Emrg1.jpg">

There has been a resurgence of emergentism that we find within [complex system theory and cognitive science][2]. One example of this, is from David Chalmers and Andy Clark’s perspective from complex systems theory. Chalmers and Clark argued that the variables involved in the emergence of the mind are not strictly an internal affair.

## Irreducibility

The debate surrounding emergentism is primarily about irreducibility. A property is _emergent_ when it is a _novel_ property. That is, the property is emergent when its existence is unpredictable, and is a result of the relationship between two or more parts. Those parts _individually_ lack the novel behavior exhibited in the phenomena.

Paul Teller argued that a property is emergent “when it is not reducible to the non-relational properties of the parts”. In this sense, you might say that the mind is an emergent phenomenon. One that is irreducible to the individual sections of the brain. The mind forms consciousness as a result of physiological interactions.

## Mind as the Result

<div class="wp-block-image">
  <figure class="alignright"><img src="https://i2.wp.com/thephilosophicalreview.com/wp-content/uploads/2018/08/200px-phrenology1-1-1-1.jpg?w=1170&#038;ssl=1" alt="" class="wp-image-45" data-recalc-dims="1" /></figure>
</div>

The emergence of the mind is a novel property of the person. Additionally, the mind is irreducible to any given sector of the brain or body. Therefore, it seems that the immediate implication is that the mind is an emergent property. Further, emergentism received indirect support through scientific research of consciousness. For example, this article in [_Technology Review._][3] Research in anesthesia concluded “that consciousness emerges from the integration of information across large networks in the brain”. As such, it seems that the idea of emergentism has become more accepted, albeit under different titles.

## The Extended Mind Theory

Andy Clark and David Chalmers constructed their version of embodied cognition through their article <a href="https://philpapers.org/rec/CLATEM" target="_blank" rel="noreferrer noopener"><em>The Extended Mind</em> </a>. The main idea of the article is in presenting <a href="https://plato.stanford.edu/entries/self-knowledge-externalism/" target="_blank" rel="noreferrer noopener">externalism</a> under a new perspective. Clark identifies his type of externalism as “active externalism”.

The fundamental principle of _active externalism_ is in the identification of the mind’s dependence on external physical objects. Clark uses three examples that consist of individuals conducting a type of cognitive action that corresponds to an action within a computer system;

<blockquote class="wp-block-quote">
  <p>
    (1) A person sits in front of a computer screen which displays images of various two-dimensional geometric shapes and is asked to answer questions concerning the potential fit of such shapes into depicted &#8216;sockets&#8217;. To assess fit, the person must mentally rotate the shapes to align them with the sockets.
  </p>

  <p>
    (2) A person sits in front of a similar computer screen, but this time can choose either to physically rotate the image on the screen, by pressing a rotate button, or to mentally rotate the image as before. We can also suppose, not unrealistically, that some speed advantage accrues to the physical rotation operation.
  </p>

  <p>
    (3) Sometime in the cyberpunk future, a person sits in front of a similar computer screen. This agent, however, has the benefit of a neural implant which can perform the rotation operation as fast as the computer in the previous example. The agent must still choose which internal resource to use (the implant or the good old-fashioned mental rotation), as each resource makes different demands on attention and other concurrent brain activity. How much cognition is present in these cases?
  </p>
</blockquote>

Clark suggests that all three of the examples consist of the same type and amount of cognitive actions. Further, Clark argues that the justification for classifying case (2) as distinctly different due to the “skin/skull” boundaries, is unfounded and arbitrary.

### Cognitive Offloading

Toward the latter portion of the article, Clark uses another to illustrate similar cognitive actions through organic and external resources, respectively,

<blockquote class="wp-block-quote">
  <p>
    First, consider a normal case of belief embedded in memory. Inga hears from a friend that there is an exhibition at the Museum of Modern Art, and decides to go see it. She thinks for a moment and recalls that the museum is on 53rd Street, so she walks to 53rd Street and goes into the museum. It seems clear that Inga believes that the museum is on 53rd Street, and that she believed this even before she consulted her memory. It was not previously an occurrent belief, but then neither are most of our beliefs.
  </p>

  <p>
    The belief was somewhere in memory, waiting to be accessed. Now consider Otto. Otto suffers from Alzheimer&#8217;s disease, and like many Alzheimer&#8217;s patients, he relies on information in the environment to help structure his life. Otto carries a notebook around with him everywhere he goes. When he learns new information, he writes it down. When he needs some old information, he looks it up.
  </p>

  <p>
    For Otto, his notebook plays the role usually played by a biological memory. Today, Otto hears about the exhibition at the Museum of Modern Art, and decides to go see it. He consults the notebook, which says that the museum is on 53rd Street, so he walks to 53rd Street and goes into the muse
  </p>

  <cite>Andy Clark and David Chalmers, <em>The Extended Mind</em></cite>

</blockquote>

Andy Clark’s example challenges standard views of the mind. Clark illustrates how mental processes are not limited to the organic memory process of the mind. Instead, the mind _couples_ with external resources. Thus, external objects play a direct role in belief formation. This conclusion suggests that external factors have causal roles in the formation of mental events.

## Coupling the Mind

Through _coupling_, which is the term that Clark uses to illustrate the event of causality between object and mind, individuals become a component or _subsystem_ within a system. According to Clark, “when it comes to belief, there is nothing sacred about skull and skin.

What makes some information count as a belief is the role it plays, and there is no reason why the relevant role can be played only from inside the body”. Otto became coupled with his notebook to such an extent that the mind became dependent upon that notebook to serve a function of its memory processes.

Clark’s argument for the extended mind theory targets the role of belief in decision-making. Specifically, when the system of forming belief consists of both external and internal resources. The article provides an extensive review of potential arguments that against the theory. Yet, the rebuttals provided seek to dissuade readers through comparison, rather than concrete negation. A comparison of individual minds is necessarily subjective. Therefore, comparisons are enlightening, but seem to fall short of making any headway.

### Further reading and references:

Broad, Charlie Dunbar. The mind and its place in nature. Routledge, 2014.

Clark, Andy, and David Chalmers. “The extended mind.” analysis 58.1 (1998): 7-19.

Kim, Jaegwon. “‘Downward causation’in emergentism and nonreductive physicalism.” Emergence or reduction (1992): 119-138.

Logan, Robert K. The extended mind: The emergence of language, the human mind, and culture. University of Toronto Press, 2007.

Mill, John Stuart. A system of logic, ratiocinative and inductive: Being a connected view of the principles of evidence and the methods of scientific investigation. Vol. 1. Longmans, green, and Company, 1884.

Sperry, Roger W. “Mind-brain interaction: Mentalism, yes; dualism, no.” Commentaries in the Neurosciences. 1980. 651-662.

Teller, Paul. “A contemporary look at emergence.” Emergence or reduction (1992): 139-153.

[1]: https://www.earlymoderntexts.com/assets/pdfs/mill1843book3_1.pdf

[2]: https://www.iep.utm.edu/emergenc/

[3]: https://www.technologyreview.com/s/426432/the-mystery-behind-anesthesia/
