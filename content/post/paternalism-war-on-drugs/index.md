+++
title = "American Paternalism and the War on Drugs"
date = 2018-10-25T00:00:00
authors = ["D. A. Berg"]
tags = ["Social and Political Philosophy"]

# List format.
#   0 = Simple
#   1 = Detailed
#   2 = Stream
list_format = 2

# Optional featured image (relative to `static/img/` folder).
[header]
image = ""
caption = ""
+++

## Background

The regulation of drug use in the United States has ramifications which, although they are largely unexpected, they are nonetheless disastrous for millions of people. Drug use and drug regulations have long histories within the United States. Drug use before federal regulations was typically limited to home remedies for pain, or recreational use. According to David Musto, in his article “_Opium, Cocaine and Marijuana in American History_”_,_ “During the 19th century, certain mood-altering substances, such as opiates and cocaine, were often regarded as compounds helpful in everyday life” (41). Musto also identifies the personal use of drugs by people such as Benjamin Franklin, who “regularly took laudanum-opium in alcohol extract-to alleviate the pain of kidney stones during the last few years of his life” (42).

Drug regulations started in local governments far before federal regulations were introduced. However, the focus of this article is on the federal regulations and their resulting ramifications. In the chronology of drug regulations in the United States, a potential starting point is the 1906 Food and Drug Act, which did not necessarily aim to prohibit the sale or use of drugs. However, the law did require that all ingredients be listed on the label. The result is a steep decline in the use of medicines which contained substances that people thought may be harmful.

The next legislative push toward drug regulation is the Harrison Tax Act, passed in 1914. This law was the result of movements by groups who wanted a ban on certain drugs. However, the notion of individual freedom was still prominent during this time, and Congress knew that a move to limit those freedoms would prove controversial. The Harrison Tax Act served as the passive-aggressive method that would avoid the controversy, as well as make drugs far more difficult to obtain.

Nearly 60 years following the Harrison Tax Act, the United States would pass into legislation laws that will serve as the basis for my argument against drug regulations: the 1970’s Controlled Substance Act. There are a multitude of reasons why this law was passed, to include political gamesmanship, health concerns, and the general public outlook on drugs. The legislation in the Controlled Substance Act created the laws which now govern and categorize various drugs according to their potential for abuse, their medical utility, and their safety. This system of categorization is known as “Scheduling” (DEA, 2018).

## Arguments for Drug Regulation

Advocates of drug regulation turn to paternalistic theories which justify their arguments against drug use, and thusly justify federal drug regulation. Paternalism, in general, is the idea that person A can intervene upon person B’s rights if B is in the process of harming himself, or someone else. Another apt description of paternalism comes from Joel Feinberg’s article “Legal Paternalism”:

> The principle of legal paternalism justifies state coercion to protect individuals from self-inflicted harm, or in its extreme version, to guide them, whether they like it or not, toward their own good. Parents can be expected to justify their interference in the lives of their children (e.g. telling them what they must eat and when they must sleep) on the ground that &#8220;daddy knows best.&#8221; (105)
>
> <cite>Joel Feinberg,&nbsp;<em>Legal Paternalism</em></cite>
However, one primary facet of the paternalistic argument is not that the individual will do harm to themself, but rather that they will potentially cause harm to others.

The moral basis for arguments against drug use is the connection between drugs and injuries or fatalities, regardless of any malicious intent. Proponents from this moral basis use examples which illustrate the connection between drug abuse, and crime. According to the Bureau of Justice Statistics, “In 2004, 17% of state prisoners and 18% of federal inmates said they committed their current offense to obtain money for drugs. These percentages represent a slight increase for federal prisoners (16% in 1997) and a slight decrease for state prisoners (19% in 1997)” (BJS, 2004).

Another paternalistic argument comes from advocates who claim that the decriminalization of drugs in the United States will result in a sudden burst of addictions and drug abuse. One such advocate, William J. Bennet, uses this belief as the premise of his counterargument with Milton Friedman. In his rebuttal, Bennett claims that we do not know with how the future would look after the legalization of drugs. Further, he argues that“we do know, however, that wherever drugs have been cheaper and more easily obtained, drug use— and addiction—has skyrocketed” (Bennet, 1989).

The paternalist also argues that the legalization of drugs will lead to an increase of miscarriages and baby health problems associated with drug use during pregnancies, as mothers with addictions will be more capable of acquiring drugs. One such argument, comes from the organization March of Dimes:

> Women who use street drugs may use more than one drug and may have other unhealthy behaviors, too. For example, they may smoke or drink alcohol. They may not eat healthy meals. They may be more likely to get a sexually transmitted disease. All of these can cause problems during pregnancy”
>
> <cite>March of Dimes,&nbsp;<em>Street Drugs and Pregnancy</em></cite>
Indeed, the damages caused by drug abuse during pregnancy need to be addressed. However, the damages caused by drug regulations far outweigh the necessity of criminalizing pregnant women. I turn now to address the arguments against drug regulation.

## Arguments Against Drug Regulation

As the situation currently stands, arguments against the “war on drugs” have become increasingly popularized, with a large amount of advocates and even politicians to weigh in. The larger portion of the arguments presented do not necessarily challenge the morality of drug use. Instead, the arguments are typically oriented toward rejecting paternalistic drug regulation. For example, alcohol remains legal despite having calculable evidence showing that the harm it causes far exceeds any evidence of the harm marijuana causes.

The open letters between Milton Friedman and William J. Bennett, serve as a juxtaposition between the two different ideologies and arguments. In this section, Milton Friedman’s letter illustrates various concerns with the illegality of drug use. Friedman identifies&nbsp;that the criminal connection to drug use is circular in nature. Violent crime that resulted in efforts to acquire a drug, appear to have only turned violent because of the laws that prohibit the open sale of the drug. Friedman argues that the legalization of various drugs would necessarily reduce crime, lower government spending, and cease the creation of drugs which turn to be more harmful than the one they are replacing.

Another rejection of paternalism is the one against the notion that legalizing drugs would lead to the availability of drugs to adolescents. This argument argues that there is no evidence suggesting that the availability post-legalization is any more feasible than the availability of currently legal drugs. Children will continue to have limited access to drugs much in the same way that they currently have limited access to tobacco and alcohol.

Perhaps the most commonly touted theory by anti-regulation proponents, is the theory of John Stuart Mill. Mill’s writings explain the necessity of individual liberty with society. In his book “On Liberty”, Mill argues against the paternalism expressed by Aristotle. One principle in Mill’s argument, and the basis for the anti-regulation argument, is called the “harm principle”. This principle is commonly reference to the introduction of Mill’s book, where he writes that “the only purpose for which power can be rightfully exercised over any member of a civilized community,&nbsp; against his will, is to prevent harm to others” (Mill, 22).

Thus, the foundations of the arguments against drug regulation consist of the right to individual liberty, and to identify the fallacies of the opposition. However, the arguments for drug regulation do present worthwhile considerations. Not least of which is the potential damage caused to youth and pregnant mothers. Increasing drug availability may have potential negative&nbsp;effects upon them, and as such the argument requires a definite solution before the legalization can be justified.

## International Pain for National Addictions

As mentioned in the closing paragraph of the last section, the necessity of giving consideration to the future of our society is detrimental to the justification of legalizing drugs. However, it appears that this consideration was not given to the potential damage to other countries in at the implementation of the Controlled Substances Act, nor by either sides of the issue. Indeed, arguments on both sides have missed a detrimental point which only alludes to the selfish nature of the issue at hand. The argument I present in this section discusses those damages, and adds to the necessity of legalizing drugs before the cost of American addiction can push Central and South America to further devastation.

While serving in the United States Marine Corps, I was sent to countries of Guatemala, Belize, and Honduras in an advisory role. While the explicit mission was to directly advise the military leaders on infantry tactics, there was an implicit&nbsp; purpose that my team and I all understood; to create a more capable force to combat drug trafficking and bring the countries to a state of reparation.

While these countries are not the worst-case scenarios of narco-states, they showed signs that have brought me to the following conclusion. The United States’ addiction to drugs paired with the illegality of drug use, has cost other nations far more than our own. The rise of the drug cartels can be attributed to a multitude of variables, such as a countries inability to appropriately handle transnational organized crime, or the internal corruption of government.&nbsp;However, a undeniable cause is that the American demand for the drugs gave rise to the demand for a cartel. It is, therefore, our duty and responsibility to establish a solution which will allow the people of Central and South America to have the same freedoms that we demand for ourselves.

## Where we are now

The drug debate rages on and will likely continue to do so. The pro regulation arguments advocate for paternalism due to the perceived responsibility to protect others, even from themselves. The other side, counters this by identifying the innate flaws in allowing substances such as tobacco and alcohol to remain legal. Further, the anti regulation arguments identify that the crime statistics within the United States and the correlation to drug use, are circular. However, both sides miss an important fact about the pressure that is placed on other countries by American addiction to illegal substances. It is for this latter reason that I believe the American government needs to legalize drugs and allow the countries ravaged by cartels to begin the journey toward reparations.