+++
title = "Dynamical Emergence of the Mind"
date = 2018-12-02T00:00:00
authors = ["D. A. Berg"]
tags = ["Philosophy of Mind", "Consciousness", "Mind-body"]

# List format.
#   0 = Simple
#   1 = Detailed
#   2 = Stream
list_format = 2

# Optional featured image (relative to `static/img/` folder).
[header]
image = ""
caption = ""
+++

# Emergentism

As you begin reading this, consider momentarily how you initially perceived the screen in front of you. For the sake of the following argument, I will assume that at no point did you initially perceive the screen as a conglomeration of light and dark areas, arranged to convey no particular meaning. Presumably, you perceived it as a singular thing, an extension of the author’s ideas on the topic of emergentism. Yet, the screen is, nonetheless, a conglomeration of light and dark areas.&nbsp;This is analogous to the most basic theory of emergence. Emergentism is one of many theories which aim to solve the “_mind-body problem”_. More precisely, these theories aim to identify the relationship between the _mental_ and the _physical_.&nbsp;

Returning to the example, we give the relationship between the areas particular meanings (letters, shapes, etc.). Then the meanings join to form a higher-order of meanings, such as words and sentences. Finally, you interpret the author&#8217;s meaning as it&nbsp;_emerges_ from the various parts of lower-order meanings. Like this example, the mind emerges from the relationship of various parts. Those various parts emerge from molecular structures, and so on.&nbsp;

<figure>
  <img src="/img/Dyn_Emrg1.jpg">
  <figcaption>(Photo taken by Ryan Somma)</figcaption>
</figure>

For more in-depth information about emergentism, see a previous article [here][1].

So, what is &#8220;dynamical emergentism,&#8221; and how does it differ from traditional theories of emergentism?

* * *

<h1 style="text-align:left">
  Dynamical Emergentism
</h1>

Timothy O&#8217;Connor&#8217;s essay&nbsp;_[The Metaphysics of Emergence][2]_&nbsp;is one of the more noteworthy pieces on Dynamical Emergentism. O&#8217;Connor goes through some of the background that led to the development of the theory. In short, traditional theories of emergentism fail due to:

  * Vague or incorrect definitions of&nbsp;_novel&nbsp;_ _causality_&nbsp;that bring causal efficacy into question.
  * Dependencies on&nbsp;_synchronic supervenience._
  * Emphases on emergence being a matter for _a priori analysis_.&nbsp;

Therefore, O&#8217;Connor claims that an approach to emergentism ought to contain&nbsp;_diachronic_ and&nbsp;_causal_ relationships between emergents.&nbsp;

<h2 style="text-align:center">
  Diachronic Vs. Synchronic
</h2>

Previous theories of emergentism that included synchronic&nbsp;[_supervenience_][3]&nbsp;failed due to an apparent &#8220;causal circularity,&#8221; as argued by [Jaegwon&nbsp;Kim][4]. O&#8217;Connor described synchronic emergence as such:

> With the synchronic variety, the whole’s having emergent&nbsp; property M1 at t1 both supervenes on its physical state >P1 at t1 and is partly causally determinative of that very physical state.
>Jaegwon Kim,&nbsp;<em>Making Sense of Emergence</em></cite>
<figure>
  <img src="/img/ds1.jpg" class="img-responsive">
  <figcaption>Photo by Brent Davis,&nbsp;[_Inventions in Teaching: A Genealogy_][5]</figcaption>
</figure>

So, although O&#8217;Connor denies that synchronic emergence necessarily entails causal circularity, he does agree that it does not fully capture the meaning of emergence.&nbsp;

Alternatively, emergence considered through a diachronic perspective considers the relationship of both the present states between each other, and the present states with future states.

> _With the diachronic variety, the whole’s having emergent property M at t1 supervenes on its physical state P1 at t1 and is partly causally determinative of its physical state P2 and mental state M2 at the subsequent time t2._
>Jaegwon Kim,&nbsp;<em>Making Sense of Emergence</em>

Kim argues that the diachronic is equally susceptible to failure, but rather due to &#8220;causal exclusion.&#8221; Although, similar to what O&#8217;Connor claims, Kim&#8217;s argument against diachronic emergence is a stretch, and ultimately boils down to a concern of redundancy.

<h2 style="text-align:center">
  Dynamic and Causal Relationships
</h2>

> Of central importance is to recognize that the relationship of micro-level structures and macro-level emergent properties is dynamic and causal, not static and formal
>
> <cite>Timothy O&#8217;Connor,&nbsp;<em>The Metaphysics Of Emergence</em></cite>

Importantly so, O&#8217;Connor&#8217;s revision of traditional emergentism is one of many new attempts to revitalize the theory. Additionally, one thing that distinguishes this theory from others, other than being far more holistic, is the emphasis on&nbsp;_upward causation_. The inclusion of this type of causality introduces emergent properties diachronically.<figure>

![The Metaphysics Of Emergence](/img/dyn-evo.png)

Above all, most traditional theories of mind posit images of mental states as singular and static. The dynamical emergence theory, it seems, makes the greatest strides toward acknowledging the levels of complexity involved with the mind.



### References and Literature Suggestions:

<p style="margin-left: 20px; text-indent: -20px;">
  O’Connor, Timothy, and Hong Yu Wong. “The Metaphysics of Emergence.” Noûs 39, no. 4 (December 1, 2005): 658–78. https://doi.org/10.1111/j.0029-4624.2005.00543.x
</p>

<p style="margin-left: 20px; text-indent: -20px;">
  Kim, Jaegwon. “Emergence: Core Ideas and Issues.” Synthese 151, no. 3 (August 1, 2006): 547–59. https://doi.org/10.1007/s11229-006-9025-0.
</p>

<p style="margin-left: 20px; text-indent: -20px;">
  Kim, Jaegwon. “Making Sense of Emergence.” Philosophical Studies 95, no. 1 (August 1, 1999): 3–36. https://doi.org/10.1023/A:1004563122154.
</p>

 [1]: https://thephilosophicalreview.com/emergentism-and-the-extended-mind/
 [2]: https://onlinelibrary.wiley.com/doi/abs/10.1111/j.0029-4624.2005.00543.x
 [3]: https://plato.stanford.edu/entries/supervenience/
 [4]: https://link.springer.com/article/10.1023%2FA%3A1004563122154
 [5]: https://amzn.to/2OLcWrE
