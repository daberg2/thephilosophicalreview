+++
title = "The Gettier Problem"
date = 2018-11-15T00:00:00
authors = ["D. A. Berg"]
tags = ["Epistemology", "Analysis of Knowledge"]

# List format.
#   0 = Simple
#   1 = Detailed
#   2 = Stream
list_format = 2

# Optional featured image (relative to `static/img/` folder).
[header]
image = "gettier.jpg"
caption = ""
+++

>I shall argue that the traditional analysis of knowledge is false in that the conditions stated therein do not constitute a sufficient condition for the truth of the proposition that S knows that P.
>
>--<cite>Edmund Gettier, *[Is Justified True Belief Knowledge?][1]*</cite>

## Edmund Gettier

There is not much to be said about modern epistemology before 1963. For that was the year that Edmund Gettier published his paper&nbsp;[_Is Justified True Belief Knowledge?_][1], which challenged the traditional analysis of knowledge. Gettier presented two cases in which a true belief is inferred from a justified false belief. He observed that, intuitively, such beliefs cannot be knowledge; it is merely lucky that they are true. However, before we can dive into the “Gettier Problem”, let’s rehash what Gettier was challenging in the first place.

## Justified True Belief

There are three components to the traditional analysis of knowledge that is semi-commonly referred to as the “Tripartite Analysis of Knowledge”. According to this analysis, knowledge consists of a justified, and true belief.

### The Tripartite Analysis

S knows that p if and only if:

  1. p is true; (The “truth” condition)
  2. S believes that p; (The “belief” condition)
  3. S is justified in believing that p (The “justification” condition)

Although there seems to be a few different versions that circulated academia during Gettier’s time, this version was the most accepted. Furthermore, this version was the one that Gettier was challenging. So, what does each condition entail?

### The Condition of Truth

First of all, it is important to understand that truth is a&nbsp;_metaphysical_&nbsp;notion, rather than epistemological. Truth exists regardless of perspectives. As such, truth is definitively objective. Where the waters get muddied is with human error. Nonetheless, false propositions cannot be knowledge. Therefore, knowledge requires truth.

### The Condition of Belief

Although this condition would presumably appear to be the simplest, it is, in my opinion, a bit more difficult to pin down. In short, the general notion behind the condition of belief is that you cannot know something that you do not believe. For example, a geologist points to a rock and says that it is, in fact, a rock. The person that the geologist is speaking to happens to be the world’s greatest skeptic, whom doesn’t believe the geologist’s proposition. Thus, the skeptic&nbsp;_doesn’t_&nbsp;know that the rock was, in fact, a rock.

### The Condition of Justification

Justification serves as the grounding point for potential cases of knowledge. However, it also serves as the point of attack for many theorists, including Edmund Gettier. Suppose that I am able to convince you of a proposition that I happen to believe is false. Solely through my sophistry, you come to believe my false proposition. However, as it turns out, my proposition was true. So, your “knowledge” is true, and you believe it. Unfortunately, though, it is grounded in a lie. In a sense, this means that you came to “know” my proposition through chance, and thus is unjustified.

## The Gettier Counter-Example

In his essay, Gettier goes through a few examples that challenge the traditional analysis. Here is one:

> Suppose that Smith and Jones have applied for a certain job. And suppose that Smith has strong evidence for the following conjunctive proposition: (a) Jones is the man who will get the job, and Jones has ten coins in his pocket.
>
> Smith’s evidence for (a) might be that the president of the company assured him that Jones would in the end be selected, and that he, Smith, had counted the coins in Jones’s pocket ten minutes ago. Proposition (a) entails: (b) The man who will get the job has ten coins in his pocket.
>
> Let us suppose that Smith sees the entailment from (a) to (b), and accepts (b) on the grounds of (a), for which he has strong evidence. In this case, Smith is clearly justified in believing that (b) is true.
>
> But imagine, further, that unknown to Smith, he himself, not Jones, will get the job. And, also, unknown to Smith, he himself has ten coins in his pocket. Proposition (b) is then true, though proposition (a), from which Smith inferred (b), is false. In our example, then, all of the following are true: (i) (b) is true, (ii) Smith believes that (b) is true, and (iii) Smith is justified in believing that (b) is true. But it is equally clear that Smith does not KNOW that (b) is true; for (b) is true in virtue of the number of coins in Smith’s pocket, while Smith does not know how many coins are in Smith’s pocket, and bases his belief in (b) on a count of the coins in Jones’s pocket, whom he falsely believes to be the man who will get the job.

Edmund Gettier’s counterexamples follow the structure of:

  1. P believes the false proposition (a)
  2. P combines (a) with a different but true premise to form a new belief of (b) that, by happenstance, is true.
  3. Therefore, P has a true belief that is not satisfactory for the definition of knowledge.

The discussions revolving around Gettier’s examples have typically examined the false premises in both of the counterexamples. Those arguing against Gettier have asserted that the justification condition for knowledge, cannot include false evidence. Therefore, Smith did not have justification for his initial belief.

[1]: http://fitelson.org/proseminar/gettier.pdf